# Java

Command line applications and notes from a Java course I took.  Minecraft world saves included for fun.

Minecraft notes:

Main castle chamber is located at XYZ: 4234 / 115 / 500

If you go to the lower levels you should be able to take a minecart to the sea temple which has been dried out.  



Recently added a fireworks launcher to the golden kitty's nose, controls are inside the head. XYZ: 4206 / 73 / 706


 Contemporary glass and tile buildings on the nearby cliffs, XYZ: 4345 / 148 / 795
 
 If you continue along the coast past the sea farm, you should see the entrance to the pirate cove through the small skull.  XYZ: 4251 / 62 / 947
 
 
 On the far side of the pirate cove there is a small farm and castle built into the cliffs.  You can follow the stairs back to the main castle or continue along the trail lined with blue orchids.
 
 
 The shipbuilding cove is around XYZ: 3809 / 77 / 880
 
 Have fun exploring our favorite world save!


